# XLAB Team: Relevantes

- Erklärungsvideos zum hier Angelegtem: https://ogy.de/utvm
- In der Readme können generelle Gedanken ausgetauscht werden
- course.md kann markdown - mäßig bearbeitet und weiter unten als PDF, Ebook oder Html ausgegeben werden.

Vorerst lasse ich das Grundgerüst zum Rumprobieren so, wie es in der course.md gerade vorliegt. 
Forkt das Projekt, arbeitet mit und pushed eure Ergebnisse zum Abgleich her.





# Vorlage für OER-Kurse

Dies ist eine Vorlage für offene und frei lizenzierte Texte, die ansprechende, und je nach Vorlage, multimediale Ergebnisse liefert und zusätzlich auch viele aufwendige Arbeiten bei der Erstellung automatisiert. Diese Vorlage kann für Kurse oder Module ebenso genutzt werden, wie für andere Text, wie z.B. Studien- oder Masterarbeiten.

* generiert Metadaten in HTML-Header für OER Repositories and Google Search
* ergänzt Lizenzhinweise nach TULLU-Regel für Wikimedia-Bilder mit maschinenlesbaren Hinweisen nach CC REL automatisch
* fügt Lizenzhinweis in generierte Dokumente ein

Mit jedem Speichern (Commit) werden die folgenden Dokumente generiert

* [Kurs als Ebook](https://mastecker.gitlab.io/calliope_xlab/course.epub)
* [Kurs als PDF](https://mastecker.gitlab.io/calliope_xlab/course.pdf)
* [Kurs als HTML](https://mastecker.gitlab.io/calliope_xlab/index.html)
