## Lösungen der Aufgabenstellungen von Julia
### Aufgabe 1
1. Der Pin heist **A1**
2. 0 bedeutet, dass der Sensor nicht gedrückt wird. Alles zwischen 1 und 999 beschreibt die Stärke des Drucks, wobei 1 gering ist 999 eher doll. Der Wert 1000 ist der höchste Wert, der erreicht wird. Dafür muss man schon doller drücken. Drückt man noch doller beschreibt der Sensor den Druck trotzdem als 1000. Es ist also der maximale Wert für starken Druck, nicht aber der absolut größte Druck. 
3. Die Zahl auf dem Display anzeigen lassen, Farben für unterschiedlichen Druck (z.B rot für 1 bis 100, blau für 101 bis 200 usw.), ein Intervall festlegen, in dem ein bestimmtes Bild/eine bestimmte Farbe angezeigt wird und immer, wenn Druck ausgeübt wird, dessen Wert nicht im Intervall ist, kommt ein anderes Bild/eine andere Farbe...

### Aufgabe 2.1
Zwischen der Brust, eher linke Seite. 30x drücken, 2x Beatmen - immer im Wechsel. Am besten im Rythmus 110x pro Minute. ,,Stayin alive" singen hilft.

### Die Implementierung
Mir ist aufgefallen, dass ich eine Variable benutzt habe, damit pro IF-Durchgang nur einmal der aktuelle Druck gemessen wird... Ich weiß nicht, ob das die Kinder verstehen. 
Folgende Implementation ist als erster Teil der Programmierung zu verstehen. Die Kinder haben es geschafft, dass der Drucksensor benutzt wird, um den richtigen Druck anzuzeigen.
Wenn zu wenig Druck ausgeübt wird erscheint ein "+", wenn es genau richtig ist ein Herz und wenn es zu viel Druck ist ein "-". 
![Code für Aufgabe 2](Images/image2.png "Code für Aufgabe 2")

Als nächstes wurde die Anzahl der Massagen gezählt und eine Anzeige eingebaut, die anzeigt, wenn genug gedrückt wurde, um 2x zu beatmen. 
![Mehr Code für Aufgabe 2](Images/image3.png "Code für Aufgabe 2")

Natürlich sind noch einige Ungenauigkeiten dabei, die aber nicht weiter stören, so zum Beispiel der fehlende Rythmus. Das bleibt aber für die Zusatzaufgaben übrig. 

Das könnte man zum Beispiel implementieren, indem man sagt, dass man immer nur dann drücken darf, wenn auch etwas auf dem Calliope leuchtet (was ich jetzt nicht extra implementiere).

### Mein Fazit
Variablen scheinen ein Problem zu sein oder? Können die Kinder dieses Konzept schon oder müssen wir es einführen? Ohne Variablen gibt es in meinen Augen keine korrekte Implementierung von der Aufgabe.







