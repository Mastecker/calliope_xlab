### Besuch im FKG am 27.01.2020

#### Eckdaten:
- 27.01.2020
- 3. und 4. Stunde
- Beginn Unterricht um 9.40 Uhr
- Raum 346
- **Beginn Aufbau: 9.20 Uhr** _spätestens_
    - Vorschlag: Treffen um 8.30 Uhr bis 9.00 Uhr? Damit wir auch genügend Zeit für die Dekoration einplanen.

