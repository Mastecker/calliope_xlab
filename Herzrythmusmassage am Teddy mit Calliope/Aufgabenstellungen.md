# Die Implementierung einer Herz-Lungen-Wiederbelebung am Teddy

### Die Benutzung vom Calliope Mini und Open Roberta
1. Verbinde deinen Calliope Mini mithilfe des USB Kabels mit deinem Computer.
2. Öffne die Website **https://calliope.cc/**
3. Unter **Los geht's** findest du den Reiter **Editoren**.
4. Wähle **Open Roberta** als Editor. 
5. Schalte vom **beginner-mode** zum **expert-mode**, damit du an die Werte des Drucksensors deines Patienten kommst.
![in_den_expert_mode](Images/image1.png "In den Expertenmodus wechseln")

### Wie führe ich mein Programm auf meinem Calliope Mini aus?
Damit du dein Programm testen und benutzen kannst muss zunächst dein Programmcode auf den Calliope Mini übertragen werden. Dafür klickst du unten rechts auf den Play-Button. Du wirst aufgefordert den Speicherort für dein Programmcode anzugeben. Wähle **Mini** aus und klicke auf speichern. Beobachte deinen Calliope Mini... woran siehst du, dass dein Calliope Mini einsatzbereit ist? Lasse den Calliope Mini mit dem Computer verbunden, weil er Strom braucht, um dein Programm ausführen zu können. Viel Spaß beim Testen deines eigenen Programms!

## Aufgabenstellungen
### Aufgabe 1: Wie funktioniert der Drucksensor?
Finde heraus, wie der Drucksensor arbeitet. Der Drucksensor kennt nur Zahlen von 0 bis 1000.
1. In welchem Pin ist der Drucksensor auf deinem Calliope angeschlossen?
2. Was bedeutet es, wenn der Drucksensor die Zahlen 0 oder 1000 zurückgibt? Was bedeuten die Zahlen zwischen 0 und 1000?
3. Wie kannst du diese Informationen mithilfe des Calliope Minis anzeigen? 

### Aufgabe 2: Das Wiederbeleben eines Teddys.
#### Hintergrundinformationen der Wiederbelebung  
Informiere dich über die optimale Durchführung einer Herz-Lungen-Wiederbelebung. Du kannst die Plakate im Klassenraum verwenden oder die Website des DRKs (Deutsches Rotes Kreuz) aufrufen: **https://www.drk.de/hilfe-in-deutschland/erste-hilfe/herz-lungen-wiederbelebung/**.
1. Wie oft wird der Brustkorb des Patienten eingedrückt? An welcher Stelle muss genau gedrückt werden?
2. Wie oft wird Beatmet? 
3. Wie wechseln sich Drücken und Beatmen ab?

#### Die Umsetzung in deinem Programm
Programmiere deinen Calliope Mini, sodass es dir helfen kann deinen Patienten wiederzubeleben. Nutze dafür den Drucksensor, um die Herzdruckmassage nicht zu leicht und nicht zu doll durchzuführen. Folgende Überlegungen können dir bei der Planung deines Programms helfen:
1. Wie soll Calliope anzeigen, ob ich zu viel/zu wenig Druck ausübe? 
2. Wie definiere ich den richtigen Druck für die Herzdruckmassage? Welche Werte des Drucksensors benutze ich dafür?
2. Wie soll Calliope bescheid geben, wenn ich oft genug gedrückt habe? 
3. Wie soll Calliope bescheid geben, wenn die Beatmung dran ist?

#### Zusatzaufgaben
Das Ziel einer Herz-Lungen-Wiederbelebung ist unter anderem den Patienten wieder zum Atmen zu bringen, nachdem er einen Kreislaufstillstand hatte. Das heißt, dass er jederzeit während der Herz-Lungen-Wiederbelebung erwachen kann. 
1. Implemntiere (programmiere) ein **zufälliges** Erwachen des Teddys. Wie soll Calliope anzeigen, dass die Herz-Lungen-Wiederbelebung zum Erfolg führte und der Teddy aufgewacht ist?
2. Um die Chancen des Erwachens zu erhöhen sollte die Herzdruckmassage in einem gleichmäßigem Rythmus durchgeführt werden. Programmiere dein Calliope Mini um, sodass ein Erwachen nur dann möglich ist, wenn der optimale Rythmus eingehalten wird (recherchiere selbst, welcher Rythmus optimal ist).


